﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour, IPoolable
{
    [System.Serializable]
    public struct BulletData
    {
        public float damage;
        public float speed;
        public Sprite sprite;
    }

    public int PoolObjectsCountNeeded => 20;

    [SerializeField] protected SpriteRenderer spriteRenderer;
    [SerializeField] protected new Rigidbody2D rigidbody2D;

    protected BulletData currentData;
    protected Vector3 direction;

    protected const float lifetime = 10f;

    protected Sequence timerSequence;
    protected Character owner;

    protected void Awake()
    {
        Subsribe();
    }

    protected void Subsribe()
    {
        Events.StateComplete += OnStateComplete;
        Events.StateLose += OnStateLose;
    }

    protected void OnStateComplete()
    {
        Destoy();
    }

    protected void OnStateLose()
    {
        Destoy();
    }

    public virtual void Initialize(Vector3 startPosition, Vector3 direction, BulletData data, Character owner)
    {
        this.direction = direction.normalized;
        this.currentData = data;
        this.owner = owner;

        spriteRenderer.sprite = data.sprite;
        transform.position = startPosition;
        transform.up = direction;
        rigidbody2D.velocity = direction * currentData.speed;

        timerSequence = DOTween.Sequence().
            AppendInterval(lifetime).
            AppendCallback(() => Destoy());
    }

    protected virtual Vector3 GetPositionDelta()
    {
        return direction * Time.deltaTime * currentData.speed;
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Character":
                Character character = collision.gameObject.GetComponentInParent<Character>();
                if (character != null && !character.Equals(owner))
                {
                    character.ReceiveDamage(currentData.damage);
                    Destoy();
                }
                break;
            case "Bullet":
                Destoy();
                break;
        }
    }

    protected void Destoy()
    {
        timerSequence.Kill();
        PoolManager.Instance.Put(this);
    }
}

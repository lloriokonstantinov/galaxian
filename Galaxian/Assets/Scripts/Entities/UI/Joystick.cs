﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    [SerializeField] protected RectTransform knob = null;

    protected float minKnobX;
    protected float maxKnobX;

    protected bool isMoving;

    protected Vector2 joystickWorldPosition;
    protected float canvasScaleFactor;

    protected void Start()
    {
        canvasScaleFactor = GetComponentInParent<Canvas>().scaleFactor;
        RectTransform selfTransform = GetComponent<RectTransform>();
        joystickWorldPosition = selfTransform.anchoredPosition * canvasScaleFactor;
        minKnobX = -selfTransform.sizeDelta.x / 2;
        maxKnobX = selfTransform.sizeDelta.x / 2;
    }

    protected void OnEnable()
    {
        Reset();
    }

    protected void OnApplicationFocus(bool focus)
    {
        Reset();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isMoving = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isMoving = false;
        knob.localPosition = Vector2.zero;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (isMoving)
        {
            float localTouchX = eventData.position.x - joystickWorldPosition.x;
            float newKnobX = Mathf.Clamp(localTouchX / canvasScaleFactor, minKnobX, maxKnobX);
            knob.localPosition = new Vector2(newKnobX, 0);
        }
    }

    protected void Reset()
    {
        isMoving = false;
        knob.localPosition = Vector2.zero;
    }

    protected void Update()
    {
        if (isMoving)
        {
            Events.OnJoystickMoved(knob.localPosition.x / maxKnobX);
        }
    }
}

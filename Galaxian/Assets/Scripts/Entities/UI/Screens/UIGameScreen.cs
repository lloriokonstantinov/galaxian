﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIGameScreen : UIScreen
{
    protected override StatesManager.States screenState =>StatesManager.States.Game;

    [SerializeField] protected Text levelText = null;

    protected void OnEnable()
    {
        levelText.text = "Level: " + CounterManager.Instance.lastLevel;
    }

    public void OnFireButtonClicked()
    {
        Events.OnFireButtonClicked();
    }
}

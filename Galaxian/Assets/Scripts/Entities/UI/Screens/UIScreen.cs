﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIScreen : MonoBehaviour
{
    protected abstract StatesManager.States screenState { get; }

    public virtual void Awake()
    {
        Subscribe();
    }

    public virtual void Subscribe()
    {
        Events.StateChanged += OnStateChanged;
    }

    protected virtual void OnStateChanged(StatesManager.States state)
    {
        if(screenState == state)
        {
            Show();
        }
        else
        {
            Hide();
        }
    }

    protected virtual void Show()
    {
        gameObject.SetActive(true);
    }

    protected virtual void Hide()
    {
        gameObject.SetActive(false);
    }
}

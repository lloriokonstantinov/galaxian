﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMenuScreen : UIScreen
{
    protected override StatesManager.States screenState => StatesManager.States.Menu;

    public void OnStartButtonClicked()
    {
        StatesManager.Instance.ChangeState(StatesManager.States.Game);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICompleteScreen : UIScreen
{
    protected override StatesManager.States screenState => StatesManager.States.Complete;

    public void OnNextButtonClicked()
    {
        StatesManager.Instance.ChangeState(StatesManager.States.Menu);
    }
}

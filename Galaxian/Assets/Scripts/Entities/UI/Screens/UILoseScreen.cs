﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILoseScreen : UIScreen
{
    protected override StatesManager.States screenState => StatesManager.States.Lose;

    public void OnRestartButtonClicked()
    {
        StatesManager.Instance.ChangeState(StatesManager.States.Menu);
    }
}

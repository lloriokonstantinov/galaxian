﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    protected void Awake()
    {
        InitializeScreens();
    }

    protected void Start()
    {
    }

    protected void InitializeScreens()
    {
        CreateScreen("UIMenuScreen");
        CreateScreen("UIGameScreen");
        CreateScreen("UICompleteScreen");
        CreateScreen("UILoseScreen");
    }

    protected void CreateScreen(string key)
    {
        var screen = PrefabsManager.Instance.CreateInstance(key);
        screen.transform.SetParent(transform, false);
    }
}

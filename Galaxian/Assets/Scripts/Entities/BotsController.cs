﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BotsController : MonoBehaviour
{
    protected const float botsXSpacing = 1.7f;
    protected const float botsYSpacing = 1.2f;

    protected float formationMoveDuration = 3f;
    protected float attacksRate = 3f;

    protected int rowsCount;
    protected int columnsCount;

    protected Bot[,] botsMatrix;

    protected Vector3 defaultPosition;

    protected Sequence attackSequence;
    protected Sequence formationMoveSeqence;

    protected void Awake()
    {
        defaultPosition = transform.position;
        Subscribe();
    }

    protected void Subscribe()
    {
        Events.StateGame += OnStateGame;
        Events.StateLose += OnStateLose;
        Events.StateComplete += OnStateComplete;
        Events.BotKilled += OnBotKilled;
    }

    protected void OnStateGame()
    {
        transform.position = defaultPosition;

        GenerateLevel(CounterManager.Instance.lastLevel);
        StartFormationMove();
        AttacksProcess();
    }

    protected void OnStateLose()
    {
        ClearBots();
        KillTweens();
    }

    protected void OnStateComplete()
    {
        ClearBots();
        KillTweens();
    }

    protected void OnBotKilled(Bot bot)
    {
        for (int i = 0; i < rowsCount; i++)
        {
            for (int j = 0; j < columnsCount; j++)
            {
                if(botsMatrix[i, j] != null && botsMatrix[i, j].Equals(bot))
                {
                    botsMatrix[i, j] = null;
                }
            }
        }

        if (StatesManager.Instance.currentState == StatesManager.States.Game && !IsAliveBotExist())
        {
            StatesManager.Instance.ChangeState(StatesManager.States.Complete);
        }
    }

    protected bool IsAliveBotExist()
    {
        for (int i = 0; i < rowsCount; i++)
        {
            for (int j = 0; j < columnsCount; j++)
            {
                if (botsMatrix[i, j] != null)
                {
                    return true;
                }
            }
        }
        return false;
    }

    protected void ClearBots()
    {
        for (int i = 0; i < rowsCount; i++)
        {
            for (int j = 0; j < columnsCount; j++)
            {
                if (botsMatrix[i, j] != null)
                {
                    botsMatrix[i, j].Die();
                }
            }
        }
        botsMatrix = null;
    }

    protected void KillTweens()
    {
        attackSequence.Kill();
        formationMoveSeqence.Kill();
    }

    protected void StartFormationMove()
    {
        transform.position = defaultPosition;

        formationMoveSeqence = DOTween.Sequence().
           Append(transform.DOMoveX(4f, formationMoveDuration / 2)).
           AppendCallback(StartRegularMove);

        void StartRegularMove()
        {
            formationMoveSeqence = DOTween.Sequence().
                 Append(transform.DOMoveX(-4f, formationMoveDuration)).
                 Append(transform.DOMoveX(4f, formationMoveDuration)).
                 SetLoops(-1);
        }
    }

    protected void AttacksProcess()
    {
        attackSequence =DOTween.Sequence().
           AppendInterval(attacksRate).
           AppendCallback(RandomAttack).
           SetLoops(-1);
    }

    protected void RandomAttack()
    {
        List<Bot> readyBots = GetBotsReadyForAttack();
        if (readyBots.Count > 0)
        {
            int rand = Random.Range(0, readyBots.Count);
            Bot.AttackSide side = readyBots[rand].transform.position.x > 0 ? Bot.AttackSide.Right : Bot.AttackSide.Left;
            readyBots[rand].StartAttack(side);
        }
    }

    protected List<Bot> GetBotsReadyForAttack()
    {
        var returnedList = new List<Bot>();
        for (int i = 0; i < rowsCount; i++)
        {
            for (int j = 0; j < columnsCount; j++)
            {
                if (botsMatrix[i, j] != null && !botsMatrix[i, j].isAttackRunning)
                {
                    if (CheckNoOneUp(i, j) && (CheckNoOneLeft(i, j) || CheckNoOneRight(i, j)))
                    {
                        returnedList.Add(botsMatrix[i, j]);
                    }
                }
            }
        }
        return returnedList;

        bool CheckNoOneUp(int i, int j)
        {
            for(int k = i + 1; k < rowsCount; k++)
            {
                if(botsMatrix[k, j] != null)
                {
                    return false;
                }
            }
            return true;
        }
        bool CheckNoOneLeft(int i, int j)
        {
            for (int k = j - 1; k >= 0; k--)
            {
                if (botsMatrix[i, k] != null)
                {
                    return false;
                }
            }
            return true;
        }
        bool CheckNoOneRight(int i, int j)
        {
            for (int k = j + 1; k < columnsCount; k++)
            {
                if (botsMatrix[i, k] != null)
                {
                    return false;
                }
            }
            return true;
        }
    }

    // Sure, that is not how level generator should look like
    // in real project, but there was no description provided, and I 
    // dont have much time ¯\_(ツ)_/¯
    protected void GenerateLevel(int level)
    {
        rowsCount = 4;
        columnsCount = Mathf.Clamp(6 + level, 6, 10);

        botsMatrix = new Bot[rowsCount, columnsCount];

        float xOffset = -botsXSpacing * (columnsCount - 1) / 2f;
        float yOffset = -botsYSpacing * (rowsCount - 1) / 2f;
        for (int i = 0; i < rowsCount; i++)
        {
            int rand = Random.Range(0, 3);
            string key = "SimpleBot";
            switch (rand)
            {
                case 1:
                    key = "HeavyBot";
                    break;
                case 2:
                    key = "FastBot";
                    break;
            }

            for (int j = 0; j < columnsCount; j++)
            {
                float nexX = botsXSpacing * j + xOffset;
                float nexY = botsYSpacing * i + yOffset;


                Bot bot = PoolManager.Instance.Take(key) as Bot;
                bot.transform.SetParent(transform);
                bot.transform.localPosition = new Vector2(nexX, nexY);
                botsMatrix[i, j] = bot;
            }
        }
    }
}

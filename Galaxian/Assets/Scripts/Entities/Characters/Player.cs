﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    protected const float minXBorder = -7;
    protected const float maxXBorder = 7;

    [SerializeField] protected float speed;

    protected void Awake()
    {
        Subscribe();
    }

    protected void Subscribe()
    {
        Events.JoystickMoved += OnJoystickMoved;
        Events.FireButtonClicked += OnFireButtonClicked;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        transform.position = new Vector3(0f, transform.position.y);
    }

    public override void Die()
    {
        base.Die();
        StatesManager.Instance.ChangeState(StatesManager.States.Lose);
    }

    protected void OnJoystickMoved(float position)
    {
        MovePositionX(position * Time.deltaTime * speed);
    }

    protected void MovePositionX(float delta)
    {
        transform.position += Vector3.right * delta;

        float newX = Mathf.Clamp(transform.position.x, minXBorder, maxXBorder);
        transform.position = new Vector3(newX, transform.position.y);
    }

    protected void OnFireButtonClicked()
    {
        MakeShot();
    }
}

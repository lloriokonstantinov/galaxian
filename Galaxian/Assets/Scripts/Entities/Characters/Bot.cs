﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot : Character, IPoolable
{
    public enum AttackSide
    {
        Left,
        Right
    }

    public int PoolObjectsCountNeeded => 15;

    public bool isAttackRunning { get; private set; }

    protected const float upYBorder = 8f;
    protected const float downYBorder = -9f;
    protected const float minShootingY = -3f;

    [SerializeField] protected float shootingRate;
    [SerializeField] protected float sideMovingDuration;
    [SerializeField] protected float downMovingDuration;

    protected Vector3 localPositionInFormation;
    protected Transform formationParent;

    protected Sequence sideMoveSequence;
    protected Sequence downMoveSequence;
    protected Sequence shootingSequence;

    public override void Die()
    {
        base.Die();
        KillTweens();
        isAttackRunning = false;
        PoolManager.Instance.Put(this);
        Events.OnBotKilled(this);
    }

    protected override void OnEnable()
    {
        transform.up = Vector3.down;
    }

    public virtual void StartAttack(AttackSide attackSide)
    {
        isAttackRunning = true;
        localPositionInFormation = transform.localPosition;
        formationParent = transform.parent;
        transform.SetParent(null);

        float xOffset = attackSide == AttackSide.Left ? -1 : 1;
        Vector3 offset = new Vector3(xOffset , 1f);
        sideMoveSequence = DOTween.Sequence().
            Append(transform.DOMove(formationParent.position + localPositionInFormation + offset, 1f)).
            AppendCallback(() => AttackTweens(attackSide));
    }

    protected virtual void AttackTweens(AttackSide attackSide)
    {
        AttackSideTweens(attackSide);
        AttackDownTweens();
        AttackShootingTweens();
    }

    protected virtual void AttackSideTweens(AttackSide attackSide)
    {
        float currentX = transform.position.x;
        sideMoveSequence = DOTween.Sequence().
            Append(transform.DOMoveX(attackSide == AttackSide.Left ? 4 : -4, sideMovingDuration)).
            Append(transform.DOMoveX(currentX, sideMovingDuration)).
            SetLoops(-1);
    }

    protected virtual void AttackDownTweens()
    {
        downMoveSequence = DOTween.Sequence().
            Append(transform.DOMoveY(downYBorder, downMovingDuration)).
            AppendCallback(ReturnToBase);
    }

    protected virtual void AttackShootingTweens()
    {
        shootingSequence = DOTween.Sequence().
            AppendInterval(shootingRate).
            AppendCallback(() => 
            {
                if(transform.position.y > minShootingY)
                {
                    MakeShot();
                }
            }).
            SetLoops(-1);
    }

    protected virtual void ReturnToBase()
    {
        KillTweens();
        transform.SetParent(formationParent);
        transform.localPosition = localPositionInFormation + Vector3.up * upYBorder;
        downMoveSequence = DOTween.Sequence().
            Append(transform.DOLocalMove(localPositionInFormation, 1f)).
            AppendCallback(() =>
            {
                isAttackRunning = false;
                transform.up = Vector3.down;
            });
    }

    protected void KillTweens()
    {
        sideMoveSequence.Kill();
        downMoveSequence.Kill();
        shootingSequence.Kill();
    }

    protected void Update()
    {
        if (isAttackRunning)
        {
            Transform target = PlayerManager.Instance.player.transform;
            transform.up = (target.position - transform.position).normalized;
        }
    }
}

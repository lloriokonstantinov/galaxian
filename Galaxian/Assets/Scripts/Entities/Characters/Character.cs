﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] protected Bullet.BulletData bulletData;
    [SerializeField] protected float maxHealth;

    protected float health;

    protected virtual void OnEnable()
    {
        health = maxHealth;
    }

    public virtual void ReceiveDamage(float damage)
    {
        health -= damage;
        if(health <= 0)
        {
            Die();
        }
    }

    public virtual void Die()
    {

    }

    protected virtual void MakeShot()
    {
        Bullet bullet = PoolManager.Instance.Take("Bullet") as Bullet;
        bullet.Initialize(transform.position, transform.up, bulletData, this);
    }
}

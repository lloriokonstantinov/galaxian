﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatesManager : MonoBehaviour
{
    #region Singletone
    public static StatesManager Instance
    {
        get
        {
            if (instance == null)
            {
                Initialize();
            }
            return instance;
        }
    }
    protected static StatesManager instance;

    public static void Initialize()
    {
        instance = new GameObject("StatesManager").AddComponent<StatesManager>();
    }
    #endregion

    public enum States
    {
        None,
        Menu,
        Game,
        Lose,
        Complete
    }

    public States previousState { get; private set; }
    public States currentState { get; private set; }

    public void ChangeState(States state)
    {
        previousState = currentState;
        currentState = state;

        Events.OnStateChanged(state);
        switch (state)
        {
            case States.Complete:
                Events.OnStateComplete();
                break;
            case States.Game:
                Events.OnStateGame();
                break;
            case States.Lose:
                Events.OnStateLose();
                break;
            case States.Menu:
                Events.OnStateMenu();
                break;
        }
    }

    protected void Awake()
    {
        previousState = States.None;
        currentState = States.Menu;
    }
}

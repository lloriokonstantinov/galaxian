﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    #region Singletone
    public static PlayerManager Instance
    {
        get
        {
            if (instance == null)
            {
                Initialize();
            }
            return instance;
        }
    }
    protected static PlayerManager instance;

    public static void Initialize()
    {
        instance = new GameObject("PlayerManager").AddComponent<PlayerManager>();
    }
    #endregion

    public Player player { get; private set; }

    protected void Awake()
    {
        player = PrefabsManager.Instance.CreateInstance("Player").GetComponent<Player>();
        player.gameObject.SetActive(false);
        Subscribe();
    }

    protected void Subscribe()
    {
        Events.StateGame += OnStateGame;
        Events.StateComplete += OnStateComplete;
        Events.StateLose += OnStateLose;
    }

    protected void OnStateGame()
    {
        player.gameObject.SetActive(true);
    }

    protected void OnStateComplete()
    {
        player.gameObject.SetActive(false);
    }

    protected void OnStateLose()
    {
        player.gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CounterManager : MonoBehaviour
{
    #region Singletone
    public static CounterManager Instance
    {
        get
        {
            if (instance == null)
            {
                Initialize();
            }
            return instance;
        }
    }
    private static CounterManager instance;

    public static void Initialize()
    {
        instance = new GameObject("CounterManager").AddComponent<CounterManager>();
    }
    #endregion

    public int lastLevel { get; protected set; }

    protected void Awake()
    {
        Load();
        Subscribe();
    }

    protected void Subscribe()
    {
        Events.StateComplete += OnStateComplete;
    }

    protected void OnStateComplete()
    {
        lastLevel++;
        Save();
    }

    protected void Load()
    {
        lastLevel = PlayerPrefs.GetInt("lastLevel", 1);
    }

    protected void Save()
    {
        PlayerPrefs.SetInt("lastLevel", lastLevel);
        PlayerPrefs.Save();
    }
}

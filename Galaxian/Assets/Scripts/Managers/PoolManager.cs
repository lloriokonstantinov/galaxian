﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class PoolManager : MonoBehaviour
{
    #region Singletone
    public static PoolManager Instance
    {
        get
        {
            if (instance == null)
            {
                Initialize();
            }
            return instance;
        }
    }
    private static PoolManager instance;

    public static void Initialize()
    {
        instance = new GameObject("PoolManager").AddComponent<PoolManager>();
    }
    #endregion

    private Transform basePoolParent;

    private Dictionary<string, MonoBehaviour> poolablePrefabs;
    private Dictionary<string, Stack<MonoBehaviour>> pools;

    private void Awake()
    {
        basePoolParent = transform;

        poolablePrefabs = new Dictionary<string, MonoBehaviour>();
        pools = new Dictionary<string, Stack<MonoBehaviour>>();

        LoadPrefabs();
    }

    private void LoadPrefabs()
    {
        MonoBehaviour[] prefabs = Resources.LoadAll<MonoBehaviour>("Prefabs");

        foreach(var prefab in prefabs)
        {
            if(prefab is IPoolable)
            {
                IPoolable poolObj = prefab as IPoolable;
                InitObject(prefab, poolObj.PoolObjectsCountNeeded);
            }
        }
    }

    public MonoBehaviour Take(string key)
    {
        if (poolablePrefabs.ContainsKey(key))
        {
            MonoBehaviour instance;
            if (pools[key].Count != 0)
            {
                instance = pools[key].Pop();
            }
            else
            {
                var prefab = poolablePrefabs[key];
                instance = Instantiate(prefab, basePoolParent);
                instance.name = key;
            }
            instance.gameObject.SetActive(true);
            return instance;
        }
        return null;
    }

    public void InitObject(MonoBehaviour poolObject, int count)
    {
        if (!poolablePrefabs.ContainsKey(poolObject.name))
        {
            poolablePrefabs.Add(poolObject.name, poolObject);
            pools.Add(poolObject.name, new Stack<MonoBehaviour>());

            for (int i = 0; i < count; i++)
            {
                var instance = Instantiate(poolObject, basePoolParent);
                instance.name = poolObject.name;
                pools[poolObject.name].Push(instance);
                instance.gameObject.SetActive(false);
            }
        }
    }

    public void Put(MonoBehaviour poolObject)
    {
        string type = poolObject.name;
        if (!poolablePrefabs.ContainsKey(type))
        {
            InitObject(poolObject, 0);
        }
        pools[type].Push(poolObject);
        poolObject.gameObject.SetActive(false);
    }
}

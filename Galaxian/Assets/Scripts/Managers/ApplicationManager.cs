﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationManager : MonoBehaviour
{
    public void Awake()
    {
        InitalizeManagers();
        InitializeScene();
        StatesManager.Instance.ChangeState(StatesManager.States.Menu);
    }

    protected void InitalizeManagers()
    {
        StatesManager.Initialize();
        PoolManager.Initialize();
        PrefabsManager.Initialize();
        PlayerManager.Initialize();
        CounterManager.Initialize();
    }

    protected void InitializeScene()
    {
        PrefabsManager.Instance.CreateInstance("Canvas");
        PrefabsManager.Instance.CreateInstance("BotsController");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabsManager : MonoBehaviour
{
    #region Singletone
    public static PrefabsManager Instance
    {
        get
        {
            if (instance == null)
            {
                Initialize();
            }
            return instance;
        }
    }
    protected static PrefabsManager instance;

    public static void Initialize()
    {
        instance = new GameObject("PrefabsManager").AddComponent<PrefabsManager>();
    }
    #endregion

    private Dictionary<string, MonoBehaviour> allPrefabs;

    protected void Awake()
    {
        LoadPrefabs();
    }

    private void LoadPrefabs()
    {
        allPrefabs = new Dictionary<string, MonoBehaviour>();
        MonoBehaviour[] prefabs = Resources.LoadAll<MonoBehaviour>("Prefabs");

        foreach (var prefab in prefabs)
        {
            allPrefabs.Add(prefab.name, prefab);
        }
    }

    public MonoBehaviour CreateInstance(string key)
    {
        if (allPrefabs.ContainsKey(key))
        {
            return Instantiate(allPrefabs[key]);
        }
        return null;
    }
}

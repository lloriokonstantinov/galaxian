﻿using UnityEngine;

public interface IPoolable
{
    int PoolObjectsCountNeeded { get; }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static partial class Events
{
    public static event Action<float> JoystickMoved;
    public static void OnJoystickMoved(float position) => JoystickMoved?.Invoke(position);

    public static event Action FireButtonClicked;
    public static void OnFireButtonClicked() => FireButtonClicked?.Invoke();
}

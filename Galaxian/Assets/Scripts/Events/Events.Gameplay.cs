﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static partial class Events
{
    public static event Action<Bot> BotKilled;
    public static void OnBotKilled(Bot bot) => BotKilled?.Invoke(bot);
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static partial class Events
{
    public static event Action StateGame;
    public static void OnStateGame() => StateGame?.Invoke();

    public static event Action StateMenu;
    public static void OnStateMenu() => StateMenu?.Invoke();

    public static event Action StateComplete;
    public static void OnStateComplete() => StateComplete?.Invoke();

    public static event Action StateLose;
    public static void OnStateLose() => StateLose?.Invoke();

    public static event Action<StatesManager.States> StateChanged;
    public static void OnStateChanged(StatesManager.States state) => StateChanged?.Invoke(state);
}
